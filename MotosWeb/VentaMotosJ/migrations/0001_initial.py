# Generated by Django 3.1.3 on 2020-12-13 13:51

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Marca',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='Producto',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('modelo', models.CharField(max_length=50)),
                ('año', models.IntegerField(max_length=4)),
                ('cilindrada', models.IntegerField(max_length=4)),
                ('descripcion', models.IntegerField(max_length=200)),
                ('estado', models.BooleanField()),
                ('marca', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='VentaMotosJ.marca')),
            ],
        ),
    ]
