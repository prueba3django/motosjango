from django.db import models

class Marca(models.Model):
    nombre = models.CharField(max_length=50)
    
    def __str__(self):
        return self.nombre
    

class Producto(models.Model):
    modelo = models.CharField(max_length=50)
    año = models.IntegerField()
    cilindrada = models.IntegerField()
    precio = models.IntegerField()
    descripcion = models.CharField(max_length= 200)
    Nuevo = models.BooleanField()
    marca = models.ForeignKey(Marca, on_delete=models.PROTECT)
    img = models.ImageField(upload_to="motos",null=True)
    
    def __str__(self):
        return self.modelo 
 