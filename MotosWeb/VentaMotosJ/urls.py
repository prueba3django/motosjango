from django.urls import path
from .views import home, nosotros, contacto, modificarborrar, motosnuevas, motosgestion, modificarmotos,eliminarmotos,buscar
urlpatterns = [
    path('', home, name="home"),    
    path('nosotros/', nosotros, name="nosotros"),
    path('contacto/', contacto, name="contacto"),
    path('modificarborrar/', modificarborrar, name="modificarborrar"),
    path('motosnuevas/', motosnuevas, name="motosnuevas"),
    path('motosgestion/', motosgestion, name="motosgestion"),
    path('modificar-motos/<id>/', modificarmotos, name="modificarmotos"),
    path('eliminar-motos/<id>/', eliminarmotos, name="eliminarmotos"),
    path('buscar/', buscar, name="buscar"),
]
