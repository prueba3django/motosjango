from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse
from VentaMotosJ.models import Producto, Marca
from VentaMotosJ.forms import ProductoForm

# Create your views here.
def home(request):
    return render(request, 'VentaMotosJ/home.html',{}) 

def nosotros(request):
    return render(request, 'VentaMotosJ/nosotros.html',{})

def modificarborrar(request):

    productos=Producto.objects.all()

    data = {
        'productos' : productos
    }


    return render(request, 'VentaMotosJ/modificarborrar.html',data)

def contacto(request):
    return render(request, 'VentaMotosJ/contacto.html',{}) 

def motosnuevas(request):
    productos = Producto.objects.all()
    marcas = Marca.objects.all()
    data = {
        'productos': productos,
        'marcas' : marcas
    }

    return render(request, 'VentaMotosJ/motosnuevas.html',data) 

def motosgestion(request): 
    data = {
            'form': ProductoForm()

    }
    if request.method == 'POST':
        formulario =ProductoForm(data=request.POST, files=request.FILES)
        if formulario.is_valid():
            formulario.save()
            data["Mensaje"] = "Agregado Con Exito"
        else:
            data["form"] = formulario

    return render(request, 'VentaMotosJ/motosgestion.html',data)

def modificarmotos(request, id):

    producto = get_object_or_404(Producto, id=id)

    data = {
        'form': ProductoForm(instance=producto)
    }
    if request.method == 'POST':
        formulario =ProductoForm(data=request.POST,instance=producto, files=request.FILES)
        if formulario.is_valid():
            formulario.save()
            return redirect(to="modificarborrar")

        data ["form"] = formulario
    return render(request, 'VentaMotosJ/modificar.html',data )

def eliminarmotos(request,id):
    producto   = get_object_or_404(Producto, id=id)
    producto.delete()
    return redirect(to="modificarborrar") 

def buscar(request):
    if  request.GET["txt_producto"]:    
        productobus=request.GET["txt_producto"]
        productos=Producto.objects.filter(nombre__icontains=productobus)       
        return  render(request,"busqueda.html",{"productos":productos,"query":productobus})
    else:mensaje="Debeingresarunnombredeproducto"
    return HttpResponse(mensaje)
    